![Build Status](https://gitlab.com/pshutsin/acc_compare_setups/badges/master/pipeline.svg)

Simple HTML tool to compare ACC setups. Written in pure JavaScript. Check `public/index.html` for full source code

Check [https://pshutsin.gitlab.io/acc_compare_setups/](https://pshutsin.gitlab.io/acc_compare_setups/) for live version.
